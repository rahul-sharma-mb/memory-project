//                      level selector
var level = "easyLevel";
const easyFunc = async () => {
  level = "easyLevel";
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  document.getElementById("p1").innerHTML = 0;

  easy();
};
const mediumFunc = async () => {
  level = "mediumLevel";
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  document.getElementById("p1").innerHTML = 0;
  medium();
};
const hardFunc = async () => {
  level = "hardLevel";
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  document.getElementById("p1").innerHTML = 0;
  hard();
};

//game reset
const reset = () => {
  if (level == "easyLevel") {
    easyFunc();
  } else if (level == "mediumLevel") {
    mediumFunc();
  } else {
    hardFunc();
  }
};

////////////////game modes-easy/////////////////////

async function easy() {
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  let highest = window.localStorage.getItem(level);
  document.getElementById("best-score").textContent = highest;
  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
  ];

  function shuffle(array) {
    let counter = array.length,
      temp,
      index; //10

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * counter);
      // console.log("index", index);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
      ///console.log(array[index], array[counter]);
    }
    // console.log(array);

    return array;
  }

  let shuffledColors = shuffle(COLORS);

  function createDivsForColors(colorArray) {
    for (let color of colorArray) {
      // create a new div
      const newDiv = document.createElement("div");

      // give it a class attribute for the value we are looping over
      newDiv.classList.add(color);
      // newDiv.style.background = color;

      // call a function handleCardClick when a div is clicked on
      newDiv.addEventListener("click", handleCardClick);

      gameContainer.append(newDiv);
    }
  }

  let clickCounter = 0,
    matchedArray = [],
    clickedArray = [];

  async function handleCardClick(event) {
    clickCounter++;
    document.getElementById("p1").innerHTML = clickCounter;

    if (clickedArray.length == 0) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 200);
      clickedArray.push(event.target.className);
      // console.log(clickedArray);
    } else if (!clickedArray.includes(event.target.className)) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 200);
      // console.log("before step 1", clickedArray);
      clickedArray.splice(0, 1);
      clickedArray.push(event.target.className);
      // console.log("after step 1", clickedArray);
    } else if (clickedArray.includes(event.target.className)) {
      clickedArray.splice(0, 1);

      let val = document.getElementsByClassName(event.target.className);

      for (let i = 0; i < val.length; i++) {
        val[i].style.background = event.target.className;
      }
      if (!matchedArray.includes(event.target.className)) {
        matchedArray.push(event.target.className);
        matchedArray.push(event.target.className);
      }
      if (matchedArray.length == 10) {
        await finish(level, clickCounter);
        alert("well done,it took you " + clickCounter + " attempts");
      }
    }

    //console.log("you clicked", event.target.className);
  }

  // when the DOM loads
  createDivsForColors(shuffledColors);
}
// easy();
//////////////////medium///////////////////////////
const medium = async () => {
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  let highest = window.localStorage.getItem(level);
  document.getElementById("best-score").textContent = highest;

  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
  ];

  function shuffle(array) {
    let counter = array.length,
      temp,
      index; //10

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * counter);
      //console.log("index", index, counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
      // console.log(temp, array[counter]);
    }
    // console.log(array);

    return array;
  }

  let shuffledColors = shuffle(COLORS);

  function createDivsForColors(colorArray) {
    for (let color of colorArray) {
      const newDiv = document.createElement("div");

      newDiv.classList.add(color);

      newDiv.addEventListener("click", handleCardClick);

      gameContainer.append(newDiv);
    }
  }

  let clickCounter = 0,
    matchedArray = [],
    clickedArray = [];

  async function handleCardClick(event) {
    clickCounter++;
    document.getElementById("p1").innerHTML = clickCounter;

    if (clickedArray.length == 0) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      clickedArray.push(event.target.className);
      // console.log(clickedArray);
    } else if (!clickedArray.includes(event.target.className)) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      // console.log("before step 1", clickedArray);
      clickedArray.splice(0, 1);
      clickedArray.push(event.target.className);
      // console.log("after step 1", clickedArray);
    } else if (clickedArray.includes(event.target.className)) {
      clickedArray.splice(0, 1);

      let val = document.getElementsByClassName(event.target.className);

      for (let i = 0; i < val.length; i++) {
        val[i].style.background = event.target.className;
      }
      if (!matchedArray.includes(event.target.className)) {
        matchedArray.push(event.target.className);
        matchedArray.push(event.target.className);
      }
      if (matchedArray.length == 14) {
        await finish(level, clickCounter);
        alert("well done,it took you " + clickCounter + " attempts");
      }
      //console.log("jajk", clickCounter);
    }

    //console.log("you clicked", event.target.className);
  }

  // when the DOM loads
  createDivsForColors(shuffledColors);
};
//medium();
//easy();

/////////////////////Hard//////////////////////////
const hard = async () => {
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";

  let highest = window.localStorage.getItem(level);
  document.getElementById("best-score").textContent = highest;
  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
    "brown",
    "black",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
    "brown",
    "black",
  ];

  function shuffle(array) {
    let counter = array.length,
      temp,
      index;

    while (counter > 0) {
      index = Math.floor(Math.random() * counter);

      counter--;

      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  }

  let shuffledColors = shuffle(COLORS);

  function createDivsForColors(colorArray) {
    for (let color of colorArray) {
      const newDiv = document.createElement("div");

      newDiv.classList.add(color);

      newDiv.addEventListener("click", handleCardClick);

      gameContainer.append(newDiv);
    }
  }

  let clickCounter = 0,
    matchedArray = [],
    clickedArray = [];

  async function handleCardClick(event) {
    clickCounter++;
    document.getElementById("p1").innerHTML = clickCounter;

    if (clickedArray.length == 0) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      clickedArray.push(event.target.className);
      //  console.log(clickedArray);
    } else if (!clickedArray.includes(event.target.className)) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);

      clickedArray.splice(0, 1);
      clickedArray.push(event.target.className);
      // console.log("after step 1", clickedArray);
    } else if (clickedArray.includes(event.target.className)) {
      clickedArray.splice(0, 1);

      let val = document.getElementsByClassName(event.target.className);

      for (let i = 0; i < val.length; i++) {
        val[i].style.background = event.target.className;
      }
      if (
        !matchedArray.includes(event.target.className) &&
        event.target.className == clickedArray[0]
      ) {
        // console.log(event.target.className, clickedArray[0]);
        matchedArray.push(event.target.className);
        matchedArray.push(event.target.className);
      }

      if (matchedArray.length == 18) {
        await finish(level, clickCounter);
        alert("well done,it took you " + clickCounter + " attempts");
      }
    }
  }

  // when the DOM loads
  createDivsForColors(shuffledColors);
};

//set the username
const game = () => {
  document.getElementById("user").innerHTML = window.localStorage.getItem(
    "name"
  );
};

//compare and update score
const finish = (level, count) => {
  // console.log("its hit");
  // console.log(JSON.parse(window.localStorage.getItem(level)));
  // console.log(window.localStorage.getItem(level));
  if (count > window.localStorage.getItem(level)) {
    //console.log(window.localStorage.getItem(level));
    window.localStorage.setItem(level, count);
    document.getElementById("best-score").textContent = count;
  }
};
game();
easy();

// var isLoggedin = false;
////////                    login                     ///////////
async function login() {
  event.preventDefault();
  let email = document.getElementById("user-email").value;
  let password = document.getElementById("user-password").value;

  let data = {
    email: email,
    password: password,
  };

  const fetchOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },

    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",

    body: JSON.stringify(data),
  };
  fetch("http://localhost:5000/users/login", fetchOptions)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      // if (data.token) {
      //   isLoggedin == true;
      // }
      if (data.token) {
        // isLoggedin == true;

        fetch(`http://localhost:5000/score/${data.id}`)
          .then((response) => response.json())
          .then((score) => {
            console.log(score);
            //score.easyLevel.toString()
            let val = score.easyLevel.toString();
            console.log(typeof score.easyLevel.toString());
            window.localStorage.setItem("name", data.name);
            window.localStorage.setItem("token", data.token);
            window.localStorage.setItem("email", data.email);
            window.localStorage.setItem("id", data.id);

            window.localStorage.setItem("easyLevel", val);
            window.localStorage.setItem("mediumLevel", score.mediumLevel);
            window.localStorage.setItem("hardLevel", score.hardLevel);
          })
          .then(() => {
            window.location.href = "http://localhost:3000/game";
          })
          .catch((e) => console.log(e));
      } else {
        document.getElementById("error").innerHTML = "INVALID COMBINATION";
      }
    })
    .catch((error) => {
      //console.log(error);
      document.getElementById("error").innerHTML = "INVALID COMBINATION";
    });
}

/////                 logout///////////////////////////////

const logout = async () => {
  let score = {
    easyLevel: window.localStorage.getItem("easyLevel"),
    mediumLevel: window.localStorage.getItem("mediumLevel"),
    hardLevel: window.localStorage.getItem("hardLevel"),
  };
  let id = window.localStorage.getItem("id");
  const fetchOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },

    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",

    body: JSON.stringify(score),
  };
  await fetch(`http://localhost:5000/score/${id}`, fetchOptions)
    .then((res) => {
      // res.json();
      console.log(res.json());
    })
    .catch((err) => console.log(err));

  window.localStorage.clear();
  window.location.href = "http://localhost:3000/";
};
