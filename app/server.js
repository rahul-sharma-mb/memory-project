const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
var cors = require("cors");
const port = 3000;

//app.use(cors());
app.options("*", cors());
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "html");
app.engine("html", require("ejs").renderFile);

//paths
// const users = require("./routes/users.js");
// const userRoutes = users.router;
// const config = require("./config");
// const { Session } = require("inspector");

//routes and middle-wares

app.use(bodyParser.json());
app.use(express.json());
app.use(express.static(path.resolve(__dirname, "../", "public")));

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get("/", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/index.html"));
});
app.get("/users/login", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/index.html"));
});
app.get("/users/signup.html", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/signup.html"));
});
app.get("/users/signup", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/signup.html"));
});
app.get("/game", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/game.html"));
});
//app.all('*', (req, res, next) => {  res.header('Access-Control-Allow-Origin’, '*');  res.header(`Access-Control-Allow-Methods`, `PUT, GET, POST, DELETE, PATCH, OPTIONS`);  res.header(`Access-Control-Allow-Headers`, '*'');  res.header('Access-Control-Allow-Credentials’, true);  next();});
// app.get("/", (req, res) => {
//   res.sendFile(path.resolve(__dirname, "../public/login.html"));
// });
//error handling
app.use((req, res, next) => {
  res
    .status(404)
    .json({
      message: "404 not found",
    })
    .end();
});
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).json({
    message:
      "An error has occurred due to internal server error.Try again later",
  });
});
app.listen(port, () => {
  console.log(`App runing on ${port}`);
});
